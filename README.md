# 常用工具

#### 介绍

收集分享常用、好用的工具，喜欢的请右上角点个star，欢迎大家推荐实用工具

**若依框架修改器**代码已开源：https://gitee.com/lpf_project/RuoYi-MT

#### 工具列表

| 名称                    | 版本                     | 地址                                                         |
| ----------------------- | ------------------------ | ------------------------------------------------------------ |
| 若依框架修改器（Windows）| V4-20230425 | https://gitee.com/lpf_project/RuoYi-MT/releases/download/V4-20230425/若依框架修改器.exe |
| 若依框架修改器(Mac) | 4.7.7 | https://gitee.com/lpf_project/RuoYi-MT/releases/download/V4-20230425/若依框架修改器.dmg |
| 代码生成器              | V4.6.0                   | https://gitee.com/lpf_project/code-generator/releases/v4.6.0 |
| WebService测试小工具    | V1-20210929              | https://gitee.com/lpf_project/webservice-test-tool           |
| Windows应用程序卸载工具 | V1.4.7.142               | https://gitee.com/lpf_project/common-tools/releases/geek1.4.7.142 |
| jar反编译工具jd-gui     | V1.6.6                   | https://github.com/java-decompiler/jd-gui/releases           |
| 向日葵远程工具          | V11.0.0.33826（2020.12） | https://sunlogin.oray.com/download/                          |
| ToDesk远程工具          | V3.0.1                   | https://www.todesk.com/download.html                         |
| Snipaste截图贴图工具    | V2.5.6                   | https://zh.snipaste.com/download.html                        |
| Windows平台的网速监控悬浮窗软件   | V1.83                   | https://github.com/zhongyang219/TrafficMonitor/releases |4
| Windows效率工具平台    | V2.6.3           | https://u.tools/ |


注：

> 若依框架修改器

- **尽量避免在C盘运行，或者使用管理员身份运行**

- `RuoYi-Cloud`版本修改后可以重新导入修改后的`ry_config_*.sql`，如果修改的时候设置了参数，那么此时数据库地址参数、`redis`参数等都已经修改，修改时候如果没有设置参数，则导入`sql`重新设置数据库和`redis`配置即可

- `RuoYi-Cloud`版本如果修改包名是以t或者r开头的，例如`top.xxx`或`ro.xxx`，修改后启动`RuoYiGatewayApplication`会报下面的错误

  ```
  No CacheResolver specified, and no bean of type CacheManager found. Register a CacheManager bean or remove the @EnableCaching annotation from your configuration
  ```

  此时需要在`redis`模块下的`spring.factories`文件中做修改，目前猜测是转义字符的原因，有更准确的解释的请联系我，修改如下

  ```
  org.springframework.boot.autoconfigure.EnableAutoConfiguration=\\\
    top.alipay.common.redis.configure.RedisConfig,\\\
    top.alipay.common.redis.service.RedisService
  ```

  

> 代码生成器：没有全部测试，有问题可以在[issues](https://gitee.com/lpf_project/code-generator/issues)提，其中的`sql`文件生成并不全


![微信赞赏码](https://images.gitee.com/uploads/images/2021/0225/180324_51cd52c7_389553.png "wx_reward.png")